package service

import (
	grpcclient "gitlab.com/salon5/autoinfo_service/internal/controller/service/grpcClient"
	"gitlab.com/salon5/autoinfo_service/internal/controller/storage"
	"gitlab.com/salon5/autoinfo_service/pkg/db"
	"gitlab.com/salon5/autoinfo_service/pkg/logger"
)

type AutoInfoService struct {
	Logger  *logger.Logger
	Clients grpcclient.Clients
	Storage	storage.IStorage
}

func NewAutoInfoService(l *logger.Logger, client grpcclient.Clients, DB *db.Postgres) *AutoInfoService {
	return &AutoInfoService{
		Logger: l,
		Clients: client,
		Storage: storage.NewStoragePg(DB),
	}
}
