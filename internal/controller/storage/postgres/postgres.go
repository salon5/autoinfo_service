package postgres

import "gitlab.com/salon5/autoinfo_service/pkg/db"

type AutoInfoRepo struct {
	DB *db.Postgres
}

func NewAutoInfoRepo(pdb *db.Postgres) *AutoInfoRepo {
	return &AutoInfoRepo{DB: pdb}
}
