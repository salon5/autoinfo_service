package storage

import (
	"gitlab.com/salon5/autoinfo_service/internal/controller/storage/postgres"
	"gitlab.com/salon5/autoinfo_service/internal/controller/storage/repo"
	"gitlab.com/salon5/autoinfo_service/pkg/db"
)

type IStorage interface {
	AutoInfo() repo.AutoInfoStorageI
}

type StoragePg struct {
	Db           *db.Postgres
	AutoInfoRepo repo.AutoInfoStorageI
}

func NewStoragePg(db *db.Postgres) *StoragePg {
	return &StoragePg{
		Db:           db,
		AutoInfoRepo: postgres.NewAutoInfoRepo(db),
	}
}

func (s StoragePg) AutoInfo() repo.AutoInfoStorageI {
	return s.AutoInfoRepo
}
