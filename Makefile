run:
	go run cmd/app/main.go

proto-gen:
	./internal/script/gen-proto.sh

update_submodule:
	git submodule update --remote --merge

create_proto_submodule:
	git submodule add git@gitlab.com:salon5/protos.git

migrate_up:
	migrate -path migrations/ -database postgres://:addsdev@adds-uz.cgjaagroetzx.ap-northeast-1.rds.amazonaws.com:5432/business_service up

migrate_down:
	migrate -path migrations/ -database postgres://addsdev:addsdev@adds-uz.cgjaagroetzx.ap-northeast-1.rds.amazonaws.com:5432/business_service down

migrate_force:
	migrate -path migrations/ -database postgres://addsdev:addsdev@adds-uz.cgjaagroetzx.ap-northeast-1.rds.amazonaws.com:5432/business_service force 5
	

 
  