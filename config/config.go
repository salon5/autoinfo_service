package config

import (
	"os"

	"github.com/spf13/cast"
)
type Config struct {
	Environment      string
	PostgresPort     string
	PostgresHost     string
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	LogLevel		string

	CustomerServicePort string
	CustomerServiceHost string

	SalonServicePort string
	SalonServiceHost string

	AutoInfoServicePort string
	AutoInfoServiceHost string

	PGXPOOLMAX		int
}

func Load() *Config {
	c := &Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))
	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToString(getOrReturnDefault("POSTGRES_PORT", "5432"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "citizenfour"))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "autoinfo_evron"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "12321"))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))

	c.CustomerServiceHost = cast.ToString(getOrReturnDefault("CUSTOMER_SERVICE_HOST", "localhost"))
	c.CustomerServicePort = cast.ToString(getOrReturnDefault("CUSTOMER_SERVICE_PORT", "0111"))

	c.SalonServiceHost = cast.ToString(getOrReturnDefault("SALON_SERVICE_HOST", "localhost"))
	c.SalonServicePort = cast.ToString(getOrReturnDefault("SALON_SERVICE_PORT", "0011"))

	c.AutoInfoServiceHost = cast.ToString(getOrReturnDefault("AUTO_INFO_SERVICE_HOST", "localhost"))
	c.AutoInfoServicePort = cast.ToString(getOrReturnDefault("AUTO_INFO_SERVICE_PORT", "0001"))

	c.PGXPOOLMAX = cast.ToInt(getOrReturnDefault("PGX_POOL_MAX", 4))
	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_,exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
